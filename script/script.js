const pictures = document.querySelectorAll('.image-to-show');
const stopButton = document.querySelector(".stop-btn")
const resumeButton = document.querySelector(".resume-btn")
let timerId;
let counter = 0;
/*let interval;*/

function showPicture () {
    pictures.forEach(item => item.classList.add('hidden'))
    if (counter > 3) {
        counter = 0;
    }
    pictures[counter].classList.remove('hidden')
    counter++;

}

function timerForPictures () {
    timerId = setTimeout(function timer() {
        showPicture()
        timerId = setTimeout(timer, 3000)
    }, 3000)
}
timerForPictures();




stopButton.addEventListener('click', () => {
    clearTimeout(timerId);
    document.querySelector(".timer").innerHTML = ''
        stopButton.disabled = true;
    if(resumeButton.disabled === true) {
        resumeButton.disabled = false;
    }
});

resumeButton.addEventListener('click', () => {
    stopButton.disabled = false;
    resumeButton.disabled = true;
    timerForPictures()
})

/*function liveTimer () {
    let startTime = Date.now();
   let interval = setTimeout(function timer() {
       let elapsedTime = Date.now() - startTime ;
       document.querySelector(".timer").innerHTML = (elapsedTime / 1000).toFixed(2);
       interval = setTimeout(timer, 100)
    }, 100)
}*/